import * as Sugar from 'sugar';

export = class Math {
    static sum(a: number, b: number): number {
        return a + b;
    }

    static divide = (a: number, b: number): number => a / b

    static total(a: Array<number>, i: number = 0): number{
        if(i == a.length){
            return 0;
        }
        return a[i] + this.total(a, ++i);
    }

    static random = (min: number, max: number): number => Sugar.Number.random(min, max)
}