
it('adds 1 + 2 to equal 3 in Typescript', () => {
  const Math = require('../Math.ts');
  expect(Math.sum(1, 2)).toBe(3);
});

it('Gives us total of values in array of random length and with random ints', () => {
  const Math = require('../Math.ts');
  const totalMe = [];
  for (const i of [1, 2, 3, 4, 5]) {
    totalMe.push(Math.random(1, 50));
  }
  const solution = totalMe.reduce((previous, current) => previous + current, 0);
  const total = Math.total(totalMe);
  expect(total).toBe(solution);
});