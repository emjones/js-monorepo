import {
  configure
} from '@storybook/angular';


function loadStories() {

  require('../src/stories/index.ts');
  require('../src/stories/hero.ts');
  require('../src/stories/eshop.ts');
}

configure(loadStories, module);
