import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCatalogOrganismComponent } from './item-catalog-organism.component';
import { SaleItemAtomComponent } from '../../atoms/sale-item-atom/sale-item-atom.component';

describe('ItemCatalogOrganismComponent', () => {
  let component: ItemCatalogOrganismComponent;
  let fixture: ComponentFixture<ItemCatalogOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemCatalogOrganismComponent, SaleItemAtomComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCatalogOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
