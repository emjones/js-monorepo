import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import CatalogItem from '../../models/CatalogItem.model';
import { ItemServiceService } from '../../../eshop-services/item-service.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-item-catalog',
  templateUrl: './item-catalog-organism.component.html',
  styleUrls: ['./item-catalog-organism.component.scss']
})
export class ItemCatalogOrganismComponent implements OnInit, OnDestroy {
  myCartItems: CatalogItem[] = [];
  myCartItems$: Observable<CatalogItem[]>;
  myCartItemsSubscription: Subscription;

  constructor(private service: ItemServiceService) {}
  logItem = (item: CatalogItem) => {
    console.log(item);
  }
  ngOnInit() {
    this.myCartItemsSubscription = this.service.getItems().subscribe(items => {
      this.myCartItems = items;
    });
  }

  onItemDelete($event) {
    console.log('Item', $event);
    this.myCartItems = this.myCartItems.filter(
      item => item.name !== $event.name
    );
  }
  ngOnDestroy(): void {
    this.myCartItemsSubscription.unsubscribe();
  }
  onCheckCart() {
    this.service.getItems().subscribe(items => {
      this.myCartItems = items;
    });
  }
}
