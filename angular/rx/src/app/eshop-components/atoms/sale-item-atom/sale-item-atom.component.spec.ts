import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleItemAtomComponent } from './sale-item-atom.component';

describe('SaleItemAtomComponent', () => {
  let component: SaleItemAtomComponent;
  let fixture: ComponentFixture<SaleItemAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleItemAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleItemAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
