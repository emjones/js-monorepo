import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import CatalogItem from '../../models/CatalogItem.model';
import { Observable } from 'rxjs';
import Policy from '../../models/Policy';
import { CardTitle } from '../../../app-components/models/CardContent.model';

@Component({
  selector: 'app-sale-item',
  templateUrl: './sale-item-atom.component.html',
  styleUrls: ['./sale-item-atom.component.scss']
})
export class SaleItemAtomComponent implements OnInit {
  @Input() item: CatalogItem = {
    price: 100,
    name: 'Name',
    description: 'Basic description'
  };
  @Output() item$: EventEmitter<CatalogItem> = new EventEmitter<CatalogItem>();

  itemPrice = 0;
  itemName = '';
  title: CardTitle;
  policy$: Observable<Policy>;
  policy: Policy;

  constructor() {}

  onRemoveClick: VoidFunction = () => {
    console.log('onClick');
    this.item$.next(this.item);
  }

  ngOnInit() {
    // this.policy$.subscribe(p => {
    //   this.policy = p;
    // });
    this.itemPrice = this.item.price;
    this.itemName = this.item.name;
    this.title = {
      title: this.item.name,
      subtitle: this.item.price.toString()
    };
  }
}
const myCoolMutant = {};
