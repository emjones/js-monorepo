import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemCatalogOrganismComponent } from './organisms/item-catalog-organism/item-catalog-organism.component';
import { SaleItemAtomComponent } from './atoms/sale-item-atom/sale-item-atom.component';
import { PipesModule } from '../pipes/pipes.module';
import { AppComponentsModule } from '../app-components/app-components.module';
import { EshopServicesModule } from '../eshop-services/eshop-services.module';

@NgModule({
  declarations: [ItemCatalogOrganismComponent, SaleItemAtomComponent],
  exports: [ItemCatalogOrganismComponent],
  imports: [CommonModule, PipesModule, AppComponentsModule, EshopServicesModule]
})
export class EshopComponentsModule {}
