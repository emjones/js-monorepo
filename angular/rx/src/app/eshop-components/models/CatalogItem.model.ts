export default interface CatalogItem {
  name: string;
  price?: number;
  description?: string;
}
