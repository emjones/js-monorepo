import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseTemplateComponent } from './base-template.component';
import { HeroTitleAtomComponent } from '../../atoms/hero-title-atom/hero-title-atom.component';
import { HeroSubtitleAtomComponent } from '../../atoms/hero-subtitle-atom/hero-subtitle-atom.component';
import { HeroMoleculeComponent } from '../../molecules/hero-molecule/hero-molecule.component';
import { ContainerAtomComponent } from '../../atoms/container-atom/container-atom.component';

describe('BaseComponent', () => {
  let component: BaseTemplateComponent;
  let fixture: ComponentFixture<BaseTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BaseTemplateComponent,
        HeroTitleAtomComponent,
        HeroSubtitleAtomComponent,
        HeroMoleculeComponent,
        ContainerAtomComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
