import { Component, OnInit } from '@angular/core';
import Hero from '../../models/Hero.model';

@Component({
  selector: 'app-base-template',
  templateUrl: './base-template.component.html',
  styleUrls: ['./base-template.component.scss']
})
export class BaseTemplateComponent implements OnInit {
  hero: Hero = {
    title: 'Title',
    subtitle: 'Subtitle'
  };
  constructor() {}

  ngOnInit() {}
}
