export default interface Hero
{
  title: string;
  subtitle: string;
}
