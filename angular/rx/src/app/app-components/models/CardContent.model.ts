export interface CardTitle {
  title: string;
  subtitle: string;
}

export interface Image {
  source: string;
  alternative: string;
}

export default interface CardContent {
  icon?: Image;
  title?: CardTitle;
}

export class ImageFactory {
  public static placeholderImage(size: string): Image {
    return {
      source: `https://bulma.io/images/placeholders/${size}.png`,
      alternative: 'Placeholder Image'
    };
  }

  public static placeholderImageBig(): Image {
    return ImageFactory.placeholderImage('96x96');
  }
  public static placeholderImageSmall(): Image {
    return ImageFactory.placeholderImage('1280x960');
  }
}
