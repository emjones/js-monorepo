import { Component, OnInit, Input } from '@angular/core';
import { CardTitle, Image, ImageFactory } from '../../models/CardContent.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() title: CardTitle = {
    title: 'Title',
    subtitle: 'Subtitle'
  };
  @Input() icon: Image = ImageFactory.placeholderImageSmall();
  @Input() image: Image = ImageFactory.placeholderImageBig();
  constructor() {}

  ngOnInit() {}
}
