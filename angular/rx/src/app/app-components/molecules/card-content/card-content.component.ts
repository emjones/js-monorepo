import { Component, OnInit, Input } from '@angular/core';
import { Image, CardTitle, ImageFactory } from '../../models/CardContent.model';

@Component({
  selector: 'app-card-content',
  templateUrl: './card-content.component.html',
  styleUrls: ['./card-content.component.scss']
})
export class CardContentComponent implements OnInit {
  @Input() icon: Image = ImageFactory.placeholderImageSmall();
  @Input() title: CardTitle = {
    title: 'Title',
    subtitle: 'Subtitle'
  };
  constructor() {}

  ngOnInit() {}
}
