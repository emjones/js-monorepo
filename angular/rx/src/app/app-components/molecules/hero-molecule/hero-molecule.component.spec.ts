import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroMoleculeComponent } from './hero-molecule.component';
import { HeroTitleAtomComponent } from '../../atoms/hero-title-atom/hero-title-atom.component';
import { HeroSubtitleAtomComponent } from '../../atoms/hero-subtitle-atom/hero-subtitle-atom.component';
import { AppComponentsModule } from '../../app-components.module';

describe('HeroMoleculeComponent', () => {
  let component: HeroMoleculeComponent;
  let fixture: ComponentFixture<HeroMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppComponentsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
