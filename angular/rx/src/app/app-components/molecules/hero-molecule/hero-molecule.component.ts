import { Component, OnInit, Input } from '@angular/core';
import Hero from '../../models/Hero.model';

@Component({
  selector: 'app-hero',
  templateUrl: './hero-molecule.component.html',
  styleUrls: ['./hero-molecule.component.scss']
})
export class HeroMoleculeComponent implements OnInit {
  title = 'Title';
  subtitle = 'Subtitle';
  @Input() hero: Hero = {
    title: 'Standard hero',
    subtitle: 'default subtitle'
  };
  constructor() {}

  ngOnInit() {
    this.title = this.hero.title;
    this.subtitle = this.hero.subtitle;
  }
}
