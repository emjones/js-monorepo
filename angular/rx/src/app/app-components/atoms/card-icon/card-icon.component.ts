import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-icon',
  templateUrl: './card-icon.component.html',
  styleUrls: ['./card-icon.component.scss']
})
export class CardIconComponent implements OnInit {
  @Input() icon: { source: string; alternative: string } = {
    source: 'https://bulma.io/images/placeholders/96x96.png',
    alternative: 'Placeholder image'
  };
  constructor() {}

  ngOnInit() {}
}
