import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTitleAtomComponent } from './card-title-atom.component';

describe('CardTitleAtomComponent', () => {
  let component: CardTitleAtomComponent;
  let fixture: ComponentFixture<CardTitleAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTitleAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTitleAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
