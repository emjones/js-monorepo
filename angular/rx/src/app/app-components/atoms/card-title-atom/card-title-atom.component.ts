import { Component, OnInit, Input } from '@angular/core';
import { CardTitle } from '../../models/CardContent.model';

@Component({
  selector: 'app-card-title',
  templateUrl: './card-title-atom.component.html',
  styleUrls: ['./card-title-atom.component.scss']
})
export class CardTitleAtomComponent implements OnInit {
  @Input() cardTitle: CardTitle = {
    title: 'Card Title',
    subtitle: 'Card Subtitle'
  };

  constructor() {}

  ngOnInit() {}
}
