import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerAtomComponent } from './container-atom.component';

describe('ContainerAtomComponent', () => {
  let component: ContainerAtomComponent;
  let fixture: ComponentFixture<ContainerAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
