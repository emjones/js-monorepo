import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardImageAtomComponent } from './card-image-atom.component';

describe('CardImageAtomComponent', () => {
  let component: CardImageAtomComponent;
  let fixture: ComponentFixture<CardImageAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardImageAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardImageAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
