import { Component, OnInit, Input } from '@angular/core';
import { Image, ImageFactory } from '../../models/CardContent.model';

@Component({
  selector: 'app-card-image',
  templateUrl: './card-image-atom.component.html',
  styleUrls: ['./card-image-atom.component.scss']
})
export class CardImageAtomComponent implements OnInit {
  @Input() image: Image = ImageFactory.placeholderImageBig();
  source: string;
  alternative: string;
  constructor() {}

  ngOnInit() {
    this.source = this.image.source;
    this.alternative = this.image.alternative;
  }
}
