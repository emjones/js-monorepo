import { Component, OnInit, Input } from '@angular/core';
import { text } from '@angular/core/src/render3';

@Component({
  selector: 'app-hero-subtitle',
  templateUrl: './hero-subtitle-atom.component.html',
  styleUrls: ['./hero-subtitle-atom.component.scss']
})
export class HeroSubtitleAtomComponent implements OnInit {
  @Input() text: string;

  constructor() {}

  ngOnInit() {}
}
