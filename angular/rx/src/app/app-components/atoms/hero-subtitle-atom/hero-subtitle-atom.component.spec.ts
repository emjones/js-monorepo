import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSubtitleAtomComponent } from './hero-subtitle-atom.component';

describe('HeroSubtitleAtomComponent', () => {
  let component: HeroSubtitleAtomComponent;
  let fixture: ComponentFixture<HeroSubtitleAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSubtitleAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSubtitleAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
