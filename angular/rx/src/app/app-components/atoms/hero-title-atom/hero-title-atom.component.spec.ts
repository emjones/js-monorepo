import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroTitleAtomComponent } from './hero-title-atom.component';

describe('HeroTitleAtomComponent', () => {
  let component: HeroTitleAtomComponent;
  let fixture: ComponentFixture<HeroTitleAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroTitleAtomComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroTitleAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
