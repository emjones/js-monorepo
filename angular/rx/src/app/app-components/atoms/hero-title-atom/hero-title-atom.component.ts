import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hero-title',
  templateUrl: './hero-title-atom.component.html',
  styleUrls: ['./hero-title-atom.component.scss']
})
export class HeroTitleAtomComponent implements OnInit {
  @Input() text: string;
  constructor() {}

  ngOnInit() {}
}
