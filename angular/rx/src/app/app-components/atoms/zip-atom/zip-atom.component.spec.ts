import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZipAtomComponent } from './zip-atom.component';

describe('ZipAtomComponent', () => {
  let component: ZipAtomComponent;
  let fixture: ComponentFixture<ZipAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZipAtomComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZipAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
