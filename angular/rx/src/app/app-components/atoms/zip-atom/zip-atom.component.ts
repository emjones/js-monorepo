import { Component, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-zip',
  templateUrl: './zip-atom.component.html',
  styleUrls: ['./zip-atom.component.scss']
})
export class ZipAtomComponent implements OnInit {
  constructor() {}
  visible = true;

  @Output() open = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();

  ngOnInit() {}

  toggle() {
    this.visible = !this.visible;
    if (this.visible) {
      this.open.emit(null);
    } else {
      this.close.emit(null);
    }
  }
}
