import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZipAtomComponent } from './atoms/zip-atom/zip-atom.component';
import { HeroTitleAtomComponent } from './atoms/hero-title-atom/hero-title-atom.component';
import { HeroSubtitleAtomComponent } from './atoms/hero-subtitle-atom/hero-subtitle-atom.component';
import { HeroMoleculeComponent } from './molecules/hero-molecule/hero-molecule.component';
import { ContainerAtomComponent } from './atoms/container-atom/container-atom.component';
import { BaseTemplateComponent } from './templates/base-template/base-template.component';
import { CardComponent } from './molecules/card/card.component';
import { CardImageAtomComponent } from './atoms/card-image-atom/card-image-atom.component';
import { CardTitleAtomComponent } from './atoms/card-title-atom/card-title-atom.component';
import { CardIconComponent } from './atoms/card-icon/card-icon.component';
import { CardContentComponent } from './molecules/card-content/card-content.component';

@NgModule({
  declarations: [
    ZipAtomComponent,
    HeroTitleAtomComponent,
    HeroSubtitleAtomComponent,
    HeroMoleculeComponent,
    ContainerAtomComponent,
    BaseTemplateComponent,
    CardComponent,
    CardImageAtomComponent,
    CardTitleAtomComponent,
    CardContentComponent,
    CardIconComponent
  ],
  exports: [BaseTemplateComponent, CardComponent],
  imports: [CommonModule]
})
export class AppComponentsModule {}
