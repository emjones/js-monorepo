import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemServiceService } from './item-service.service';

@NgModule({
  imports: [CommonModule],
  providers: [ItemServiceService]
})
export class EshopServicesModule {}
