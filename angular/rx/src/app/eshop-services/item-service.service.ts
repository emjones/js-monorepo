import { Injectable } from '@angular/core';
import CatalogItem from '../eshop-components/models/CatalogItem.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ItemServiceService {
  private items: BehaviorSubject<CatalogItem[]>;
  private items$: Observable<CatalogItem[]>;

  constructor() {
    this.items = new BehaviorSubject<CatalogItem[]>([
      {
        name: 'My first cool Item',
        price: 1.23,
        description: 'Basic description 1'
      },
      {
        name: 'My second cool Item',
        price: 103.23,
        description: 'Basic description 2'
      }
    ]);
    this.items$ = this.items.asObservable();
  }

  getItems(): Observable<CatalogItem[]> {
    return this.items$;
  }
}
