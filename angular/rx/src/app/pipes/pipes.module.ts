import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DollarSignPipe } from './dollar-sign.pipe';

@NgModule({
  declarations: [DollarSignPipe],
  exports: [DollarSignPipe],
  imports: [CommonModule]
})
export class PipesModule {}
