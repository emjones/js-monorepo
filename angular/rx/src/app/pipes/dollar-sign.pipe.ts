import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dollarSign'
})
export class DollarSignPipe implements PipeTransform {
  transform(value: number | null): string {
    return value ? `$${value}` : '$0';
  }
}
