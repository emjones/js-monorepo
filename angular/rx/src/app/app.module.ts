import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppComponentsModule } from './app-components/app-components.module';
import { EshopComponentsModule } from './eshop-components/eshop-components.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    EshopComponentsModule,
    AppComponentsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
