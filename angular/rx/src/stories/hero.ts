import { storiesOf } from '@storybook/angular';
import { HeroTitleAtomComponent } from '../app/app-components/atoms/hero-title-atom/hero-title-atom.component';
import { HeroSubtitleAtomComponent } from '../app/app-components/atoms/hero-subtitle-atom/hero-subtitle-atom.component';
import { HeroMoleculeComponent } from '../app/app-components/molecules/hero-molecule/hero-molecule.component';
import { CommonModule } from '@angular/common';
import { ZipAtomComponent } from '../app/app-components/atoms/zip-atom/zip-atom.component';
import { ContainerAtomComponent } from '../app/app-components/atoms/container-atom/container-atom.component';
import { BaseTemplateComponent } from '../app/app-components/templates/base-template/base-template.component';
const moduleMetadata = {
  declarations: [
    ZipAtomComponent,
    HeroTitleAtomComponent,
    HeroSubtitleAtomComponent,
    HeroMoleculeComponent,
    ContainerAtomComponent,
    BaseTemplateComponent
  ],
  imports: [CommonModule]
};
storiesOf('Atoms/Hero', module)
  .add('Basic hero title', () => ({
    component: HeroTitleAtomComponent,
    props: {
      text: 'Hero Title'
    },
    moduleMetadata
  }))
  .add('Basic Hero Subtitle', () => ({
    component: HeroSubtitleAtomComponent,
    props: {
      text: 'Hero Sub Title'
    },
    moduleMetadata
  }));

storiesOf('Molecules/Hero', module).add('Basic hero title', () => ({
  component: HeroMoleculeComponent,
  props: {
    title: 'Hero Title',
    subtitle: 'Hero subtitle'
  },
  moduleMetadata
}));
