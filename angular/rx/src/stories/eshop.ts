import { storiesOf } from '@storybook/angular';
import { ItemCatalogOrganismComponent } from '../app/eshop-components/organisms/item-catalog-organism/item-catalog-organism.component';
import { SaleItemAtomComponent } from '../app/eshop-components/atoms/sale-item-atom/sale-item-atom.component';
const moduleMetadata = {
  declarations: [ItemCatalogOrganismComponent, SaleItemAtomComponent]
};

storiesOf('Eshop/Organisms', module).add('Item Catalog', () => ({
  component: ItemCatalogOrganismComponent,
  props: {
    items: [
      { name: 'My first cool Item', price: '$1.24' },
      { name: 'My second cool Item', price: '$103.23' }
    ]
  },
  moduleMetadata
}));

storiesOf('Eshop/Atoms', module).add('Sale Itom', () => ({
  component: SaleItemAtomComponent,
  props: {
    item: { name: 'Storybook Name', price: '1.20' }
  },
  moduleMetadata
}));
