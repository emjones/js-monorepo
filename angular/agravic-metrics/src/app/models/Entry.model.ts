export class Entry {
  weight: number;
  date: Date;
  bodyFat: number;
  id: number;
}

export class EntryConfig {
  showBodyFat = false;
}
