import { Component, OnInit } from '@angular/core';
import { EntryService } from './services/entry.service';
import { EntryConfig, Entry } from './models/Entry.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'agravic-metrics';

  config: EntryConfig;
  constructor(private entryService: EntryService) {}

  ngOnInit(): void {
    this.entryService.entryConfig.subscribe((config: EntryConfig) => {
      this.config = config;
    });
  }
}
