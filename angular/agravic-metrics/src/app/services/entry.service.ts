import { Injectable } from '@angular/core';
import { Entry, EntryConfig } from '../models/Entry.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
const entrySorter: (a: Entry, b: Entry) => number = (a: Entry, b: Entry) => {
  return a.date > b.date ? 1 : a.date === b.date ? 0 : -1;
};

const getMaxId: (a: Entry[]) => number = (a: Entry[]) => {
  return a.reduce((p, c) => {
    return Math.max(p, c.id);
  }, 0);
};

const responseTransform: (entryResponse) => Entry[] = entryResponse => {
  return entryResponse.map((entry: Entry) => ({
    ...entry,
    date: new Date(entry.date)
  }));
};

@Injectable({
  providedIn: 'root'
})
export class EntryService {
  public entryConfig: Observable<EntryConfig>;
  private entryConfigSubject: BehaviorSubject<EntryConfig>;

  constructor(private httpClient: HttpClient) {
    this.entryConfigSubject = new BehaviorSubject(new EntryConfig());
    this.entryConfig = this.entryConfigSubject.asObservable();
  }

  public loadEntries(): Observable<Entry[]> {
    return this.httpClient.get('/api/entries').pipe(
      map(responseTransform),
      map(entries => [...entries].sort(entrySorter))
    );
  }

  public addEntry: (a: Entry) => Observable<any> = (a: Entry) => {
    return this.httpClient.post('/api/entries', a);
  }

  public toggleBodyFat: (a: EntryConfig) => void = (a: EntryConfig) => {
    const showBodyFat: boolean = !a.showBodyFat;
    const config: EntryConfig = { ...a, showBodyFat };
    this.entryConfigSubject.next(config);
  }
}
