import { TestBed } from '@angular/core/testing';

import { EntryService } from './entry.service';
import { EntryConfig, Entry } from '../models/Entry.model';

describe('EntryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntryService = TestBed.get(EntryService);
    expect(service).toBeTruthy();
  });
  fit('should toggle body fat showing', () => {
    const service: EntryService = TestBed.get(EntryService);
    let config = new EntryConfig();
    service.entryConfig.subscribe((newConfig: EntryConfig) => {
      config = newConfig;
    });
    expect(config.showBodyFat).toBe(false); // defaults to false
    service.toggleBodyFat(config);
    expect(config.showBodyFat).toBe(true);
  });

  // entry form testing
  fit('should add new entry', () => {
    const service: EntryService = TestBed.get(EntryService);
    let entries: Entry[];
    service.sortedEntries.subscribe((entryArray: Entry[]) => {
      entries = entryArray;
    });
    expect(entries.length).toBe(6);
    const newEntry = new Entry();
    newEntry.id = -1;
    newEntry.date = new Date();
    newEntry.bodyFat = 10 / 100;
    newEntry.weight = 220;
    service.addEntry(newEntry, entries);
    expect(entries[5].id).toBe(6);
    expect(entries.length).toBe(7);
    expect(entries[6].id).toBe(7);
  });
});
