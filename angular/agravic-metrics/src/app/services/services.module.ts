import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { EntryService } from './entry.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [EntryService]
})
export class ServicesModule {}
