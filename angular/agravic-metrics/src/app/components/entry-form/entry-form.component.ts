import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Entry } from '../../models/Entry.model';

@Component({
  selector: 'app-entry-form',
  templateUrl: './entry-form.component.html',
  styleUrls: ['./entry-form.component.scss']
})
export class EntryFormComponent implements OnInit {
  @Input()
  showBodyFat = false;

  @Output() create = new EventEmitter();

  model;
  constructor() {
    this.model = {};
  }

  ngOnInit() {}

  createEntry() {
    this.model.bodyFat = this.model.bodyFat / 100;
    this.create.emit(this.model);
  }
}
