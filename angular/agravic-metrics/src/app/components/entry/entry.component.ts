import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntryService } from '../../services/entry.service';
import { Entry } from 'src/app/models/Entry.model';
const matchEntryId = id => {
  return (entry: Entry) => id === entry.id;
};
@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {
  entry: Entry = new Entry();
  constructor(
    private route: ActivatedRoute,
    private entriesService: EntryService
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.params.id || 0;
    this.entriesService.loadEntries().subscribe((entries: Entry[]) => {
      this.entry = entries.find(matchEntryId(id));
    });
  }
}
