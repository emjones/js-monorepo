import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { EntriesComponent } from './entries/entries.component';
import { EntryFormComponent } from './entry-form/entry-form.component';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AboutComponent } from './about/about.component';
import { EntryComponent } from './entry/entry.component';
@NgModule({
  declarations: [
    NavComponent,
    EntriesComponent,
    EntryFormComponent,
    HomeComponent,
    LoginComponent,
    AboutComponent,
    EntryComponent
  ],
  exports: [NavComponent, EntriesComponent, EntryFormComponent],
  imports: [CommonModule, FormsModule, RouterModule]
})
export class ComponentsModule {}
