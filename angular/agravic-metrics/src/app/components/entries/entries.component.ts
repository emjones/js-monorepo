import { Component, OnInit, Input } from '@angular/core';
import { Entry, EntryConfig } from 'src/app/models/Entry.model';
import { EntryService } from '../../services/entry.service';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.scss']
})
export class EntriesComponent implements OnInit {
  @Input()
  entries: Entry[];

  config: EntryConfig = new EntryConfig();
  constructor(private entryService: EntryService) {}

  ngOnInit() {
    this._loadEntries();
    this.entryService.entryConfig.subscribe((conf: EntryConfig) => {
      this.config = conf;
    });
  }

  private _loadEntries = () => {
    this._subscribe(this._entries(), this._subscription);
  }

  private _subscription: (entries: Entry[]) => void = (entries: Entry[]) => {
    this.entries = entries;
  }
  private _entries(): any {
    return this.entryService.loadEntries();
  }
  private _subscribe(arg0: any, arg1: any): any {
    return arg0.subscribe(arg1);
  }

  toggleBodyFat() {
    this.entryService.toggleBodyFat(this.config);
  }

  createEntry(entry: Entry) {
    this.entryService.addEntry(entry).subscribe(this._loadEntries);
  }
}
