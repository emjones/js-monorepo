import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './components/templates/base/base.component';
import { RegisterComponent } from './components/pages/register/register.component';
import { ChatComponent } from './components/pages/chat/chat.component';
import { ConversationComponent } from './components/organisms/conversation/conversation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatGridListModule,
  MatListModule,
  MatMenuModule
} from '@angular/material';

import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    BaseComponent,
    RegisterComponent,
    ChatComponent,
    ConversationComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatListModule,
    HttpClientModule,
    MatMenuModule,
    PipesModule
  ],
  exports: [RegisterComponent, ChatComponent, BaseComponent]
})
export class ChatModule {}
