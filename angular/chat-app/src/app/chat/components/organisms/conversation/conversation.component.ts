import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from '../../../../services/chat.service';
import Chat from '../../../../models/Chat.model';
import { Subscription } from 'rxjs';
import Registration from 'src/app/models/Registration.model';
import { RegisterService } from 'src/app/services/register.service';
import { CHAT_ROOMS } from '../../../../models/Registration.model';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  chatSubscription: Subscription;
  chats: Chat[];
  currentUser: Registration;
  registrationSubscription: Subscription;
  chatRooms: string[] = CHAT_ROOMS;
  constructor(
    private chatService: ChatService,
    private registrationService: RegisterService
  ) {}

  chatsLoaded: (chats: Chat[]) => void = (chats: Chat[]) => {
    this.chats = chats;
  }

  switchChatRoom: (event: { target: HTMLInputElement }) => void = (event: {
    target: HTMLInputElement;
  }) => {
    const selectedChatRoom = event.target.value;
    const { screenName } = this.currentUser;
    this.registrationService.register({
      selectedChatRoom,
      screenName
    });
  }

  ngOnInit() {
    this.registrationSubscription = this.registrationService
      .getRegistrations()
      .subscribe((reg: Registration) => {
        this.currentUser = reg;
        this.chatService.loadChats(reg.selectedChatRoom);
      });
    this.chatSubscription = this.chatService
      .getChats()
      .subscribe(this.chatsLoaded);

    this.subscriptions.push(this.registrationSubscription);
    this.subscriptions.push(this.chatSubscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
