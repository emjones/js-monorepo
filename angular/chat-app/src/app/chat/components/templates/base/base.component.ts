import { Component, OnInit, OnDestroy } from '@angular/core';
import { RegisterService } from '../../../../services/register.service';
import { Subscription } from 'rxjs';
import Registration from '../../../../models/Registration.model';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit, OnDestroy {
  registrationSubscription: Subscription;
  isValid: Boolean = false;
  constructor(private registrationService: RegisterService) {}

  ngOnInit() {
    this.registrationSubscription = this.registrationService
      .getRegistrations()
      .subscribe((user: Registration) => {
        this.isValid = user.screenName !== '';
      });
  }
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }
}
