import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RegisterService } from '../../../../services/register.service';
import Registration from 'src/app/models/Registration.model';
import { Subscription } from 'rxjs';
import { ChatService } from '../../../../services/chat.service';
import Chat from 'src/app/models/Chat.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  chatText: FormControl = new FormControl('');
  registrySubscription: Subscription;
  chatSubscription: Subscription;
  currentUser: Registration;
  chats: Chat[];
  constructor(
    private registryService: RegisterService,
    private chatService: ChatService,
    private router: Router
  ) {}

  ngOnInit() {
    this.registrySubscription = this.registryService
      .getRegistrations()
      .subscribe((registration: Registration) => {
        this.currentUser = registration;
      });
    this.chatSubscription = this.chatService
      .getChats()
      .subscribe((chats: Chat[]) => {
        this.chats = chats;
      });
  }

  ngOnDestroy(): void {
    this.registrySubscription.unsubscribe();
    this.chatSubscription.unsubscribe();
  }

  onSubmit() {
    const { screenName, selectedChatRoom } = this.currentUser;
    const chatRoom = selectedChatRoom;
    const timestamp = new Date().toISOString();
    const message = this.chatText.value;
    this.chatService.sendMessage({ screenName, chatRoom, timestamp, message });
    setTimeout(() => {
      this.router.navigate(['chat']);
    }, 2000);
  }
}
