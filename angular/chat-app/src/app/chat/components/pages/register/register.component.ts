import { Component, OnInit, OnDestroy } from '@angular/core';
import Chat from '../../../../models/Chat.model';
import Registration from 'src/app/models/Registration.model';
import { FormControl, FormGroup } from '@angular/forms';
import { ChatService } from '../../../../services/chat.service';
import { Subscription } from 'rxjs';
import { RegisterService } from 'src/app/services/register.service';
import { CHAT_ROOMS } from '../../../../models/Registration.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registrationForm = new FormGroup({
    screenName: new FormControl(''),
    selectedChatRoom: new FormControl('')
  });

  chatRooms: string[] = CHAT_ROOMS;
  chatSubscription: Subscription;

  constructor(
    private registrationService: RegisterService,
    private router: Router
  ) {}

  ngOnInit() {}
  onChange(event: Event) {}
  onSubmit() {
    // create new registration for active registration
    const reg: Registration = this.registrationForm.getRawValue();
    this.registrationService.register(reg);
    this.router.navigate(['chat']);
  }
  ngOnDestroy(): void {}
}
