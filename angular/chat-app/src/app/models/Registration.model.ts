export default interface Registration {
  screenName: string;
  selectedChatRoom: string;
}

export const CHAT_ROOMS = [
  'Fun with Taxes',
  'The Dark Web',
  'Everything NG',
  'Mystic-1-4-U'
];
