import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadableTimestampPipe } from './readable-timestamp.pipe';

@NgModule({
  declarations: [ReadableTimestampPipe],
  exports: [ReadableTimestampPipe],
  imports: [CommonModule]
})
export class PipesModule {}
