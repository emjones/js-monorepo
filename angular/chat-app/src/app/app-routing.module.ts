import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat/components/pages/chat/chat.component';
import { RegisterComponent } from './chat/components/pages/register/register.component';

const routes: Routes = [
  { path: 'chat', component: ChatComponent },
  { path: 'register', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
