import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatService } from './chat.service';
import { RegisterService } from './register.service';

@NgModule({
  imports: [CommonModule],
  providers: [ChatService, RegisterService]
})
export class ServicesModule {}
