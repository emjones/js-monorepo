import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import Chat from '../models/Chat.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class ChatService {
  readonly getChatMessageApiUri = environment.firebaseConfig.getEndpoint;
  readonly setChatMessageApiUri = environment.firebaseConfig.setEndpoint;
  private chats: BehaviorSubject<Chat[]>;
  private chats$: Observable<Chat[]>;

  constructor(private http: HttpClient) {
    this.chats = new BehaviorSubject([]);
    this.chats$ = this.chats.asObservable();
  }

  private _addChats: (chats: Chat[]) => void = (chats: Chat[]) => {
    this.chats.next(chats);
  }

  private _chatError: (error: any) => void = (error: any) => {
    console.warn(error);
  }

  public getChats(): Observable<Chat[]> {
    return this.chats$;
  }

  public loadChats(chatRoom: string): void {
    this.http
      .get<Chat[]>(`${this.getChatMessageApiUri}?room=${chatRoom}`)
      .toPromise()
      .then(this._addChats)
      .catch(this._chatError);
  }

  public sendMessage(message: Chat): void {
    this.http
      .post<Chat>(`${this.setChatMessageApiUri}`, message)
      .toPromise()
      .then(r => {
        const { chatRoom } = message;
        this.loadChats(chatRoom);
      });
  }

  public addChat(chat: Chat): void {}
}
