import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import Registration from '../models/Registration.model';

@Injectable()
export class RegisterService {
  private registration: BehaviorSubject<Registration>;
  private registration$: Observable<Registration>;
  constructor() {
    this.registration = new BehaviorSubject({
      screenName: '',
      selectedChatRoom: ''
    });
    this.registration$ = this.registration.asObservable();
  }

  public getRegistrations(): Observable<Registration> {
    return this.registration$;
  }

  public register(registration: Registration) {
    this.registration.next({ ...registration });
  }
}
