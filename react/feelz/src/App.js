import React from 'react';
import './App.css';
import RActivityCreator from "./containers/RActivityCreator";
import RCounterList from "./containers/RCounterList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <RActivityCreator/>
        <RCounterList/>
      </header>
    </div>
  );
}

export default App;
