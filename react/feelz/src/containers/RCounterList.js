import {countActivity} from '../store/actions';
import {connect} from "react-redux";
import CounterList from "../components/CounterList";
import {getActivities} from "../store/effects";

const mapState = ({activities}) => ({
  activities: activities.map,
  isLoading: activities.isLoading
});

const mapActivityActions = dispatch => ({
  count: id => dispatch(countActivity(id)),
  loadActivities: () => dispatch(getActivities())
});

export default connect(mapState, mapActivityActions)(CounterList)

