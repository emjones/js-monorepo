import {addActivity} from '../store/actions';
import {connect} from 'react-redux'
import ActivityCreator from "../components/ActivityCreator";

const mapStateToProps = null;

const mapDispatchToProps = dispatch => ({
  createActivity: name => dispatch(addActivity(name))
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityCreator)
