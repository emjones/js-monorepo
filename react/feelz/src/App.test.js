import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import rootState from "./store";
import {createStore} from "redux";
import {Provider} from "react-redux";

it('renders without crashing', () => {
  const store = createStore(rootState);
  const props = {store};
  const div = document.createElement('div');
  ReactDOM.render(<Provider {...props}><App/></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
