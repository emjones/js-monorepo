import {combineReducers} from "redux";
import {activities} from "./reducers";

export default combineReducers({activities});
