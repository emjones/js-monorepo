import {isLoadingActivities, isNotLoadingActivities, setActivities} from "./actions";
import {of} from 'rxjs';
import {Activity} from "../models/activities";

class AppClient {
  getActivities = () => {
    return of([{id: 1, name: 'Activity 1'}, {id: 2, name: 'Activity 2'}, {id: 3, name: 'Activity 3'}]);
  }
}

const client = new AppClient();
export const getActivities = () => (dispatch) => {
  dispatch(isLoadingActivities());
  client.getActivities().subscribe(activities => {
    dispatch(setActivities(activities.map(a => new Activity(a))));
    dispatch(isNotLoadingActivities());
  });
};
