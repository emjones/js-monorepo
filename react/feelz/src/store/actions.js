export const ActivityAction = {
  ADD_ACTIVITY: 'ADD_ACTIVITY',
  COUNT_ACTIVITY: 'COUNT_ACTIVITY',
  SET_ACTIVITIES: 'SET_ACTIVITIES',
  LOADING: 'LOAD_ACTIVITIES'
};

/**
 * Action Creators
 */

/**
 *
 * @param id
 * @returns {{id: *, type: string}}
 */
export const countActivity = id => ({
  type: ActivityAction.COUNT_ACTIVITY,
  id
});

/**
 *
 * @param name
 * @returns {{name: *, type: string}}
 */
export const addActivity = name => ({
  type: ActivityAction.ADD_ACTIVITY,
  name
});

/**
 *
 * @param activities
 * @returns {{activities: *, type: string}}
 */
export const setActivities = activities => ({
  type: ActivityAction.SET_ACTIVITIES,
  activities
});

export const loadingActivities = (isLoading) => ({type: ActivityAction.LOADING, isLoading});
export const isNotLoadingActivities = () => loadingActivities(false);
export const isLoadingActivities = () => loadingActivities(true);
