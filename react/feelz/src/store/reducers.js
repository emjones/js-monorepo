import {ActivityAction} from "./actions";
import {reduceToMapByPropNamedId} from "../lib/collections";

const getIds = (activities) => Object.values(activities).map(({id}) => id);
const initialState = {map: {}, isLoading: false};

export const activities = (state = initialState, action) => {
  const {type} = action;
  let newState = {};
  switch (type) {
    case ActivityAction.ADD_ACTIVITY:
      const id = Math.max(...[0, ...getIds(state.map)]) + 1;
      newState.map = {...state.map, [id]: {name: action.name, id, records: []}};
      break;
    case ActivityAction.SET_ACTIVITIES:
      newState.map = reduceToMapByPropNamedId(action.activities);
      break;
    case ActivityAction.COUNT_ACTIVITY:
      const {map} = state;
      const activity = map[action.id];
      newState.map = {...map, [action.id]: {...activity, records: [...activity.records, new Date()]}};
      break;
    case ActivityAction.LOADING:
      newState.isLoading = action.isLoading;
      break;
    default:
      break;
  }
  return {...state, ...newState};
};
