export const toMap = propName => (map, item) => ({...map, [item[propName]]: item});
export const toMultiMap = propName => (map, item) => ({...map, [item[propName]]: [...map[propName], item]});

export const reduceByPropName = (propName, type) => type(propName);

export const reduceToMapByPropNamedId = arr => arr.reduce(reduceByPropName('id', toMap), {});
