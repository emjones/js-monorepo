import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from 'redux';
import rootState from './store';
import thunk from "redux-thunk";

let enhancer;
const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
  const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      }) : compose;
  enhancer = composeEnhancers(
    applyMiddleware(...middleware),
    // other store enhancers if any
  );
} else {
  enhancer = console.log('done');
}

const store = createStore(rootState, enhancer);
const props = {store};
ReactDOM.render(
  <Provider {...props}>
    <App/>
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
