import React from 'react';
import PropTypes from 'prop-types';

const Counter = ({activity, onClick}) => {
  return (
    <div>
      <button onClick={() => onClick()}>{activity.name}</button>
    </div>
  )
};

Counter.propTypes = {
  activity: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  onClick: PropTypes.func.isRequired
};

export default Counter
