import React, {useState} from 'react';
import PropTypes from 'prop-types';

const ActivityCreator = ({createActivity}) => {
  const [activityName, setActivityName] = useState('');
  return (
    <div>
      <h2>
        Add an Activity!
      </h2>
      <div>
        <input type="text" onChange={event => setActivityName(event.target.value)}/>
        <button id="add" onClick={() => {
          setActivityName('');
          createActivity(activityName)
        }}>Add Activity
        </button>
      </div>
    </div>
  )
};

ActivityCreator.propTypes = {
  createActivity: PropTypes.func.isRequired
};

export default ActivityCreator;
