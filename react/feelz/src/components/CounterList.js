import React, {useEffect, useState} from 'react';
import Counter from './Counter';

export default ({activities, count, loadActivities}) => {
  const [initialized, setInitialized] = useState(false);
  useEffect(() => {
    if (!initialized) {
      loadActivities();
      setInitialized(true);
    }
  }, [initialized, setInitialized]);

  return (<div>
    {
      Object.values(activities)
        .map(activity => <Counter key={activity.id} activity={activity} onClick={() => count(activity.id)}/>)
    }
  </div>);
}
